package com.example.ozh.printme20.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ozh.printme20.Model.User;
import com.example.ozh.printme20.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oleg on 25.08.15.
 */
public class UserListAdapter extends RecyclerView.Adapter<ViewUserHolder>{

    private List<User> users = new ArrayList<>();
    private Context context;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public UserListAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_instagram_black_48dp)
                .showImageForEmptyUri(R.drawable.ic_instagram_grey600_48dp)
                .showImageOnFail(R.drawable.ic_instagram_grey600_48dp)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public ViewUserHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_list, parent, false);
        ViewUserHolder viewHolder = new ViewUserHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewUserHolder viewUserHolder, int i) {
        viewUserHolder.textView.setText(users.get(i).getName());
        imageLoader.displayImage(users.get(i).getProfImage(), viewUserHolder.imageView, options);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

}
