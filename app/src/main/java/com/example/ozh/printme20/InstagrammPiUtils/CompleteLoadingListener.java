package com.example.ozh.printme20.InstagrammPiUtils;

import android.graphics.Bitmap;

import com.example.ozh.printme20.Model.Image;
import java.util.List;

/**
 * Created by oleg on 25.08.15.
 */
public interface CompleteLoadingListener {
    public void onCompleteSearch(List<Image> list, SearchResult searchResult);
}
