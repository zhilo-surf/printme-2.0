package com.example.ozh.printme20.InstagrammPiUtils;

import android.view.View;

/**
 * Created by oleg on 25.08.15.
 * Listener для RecycleView
 */
public interface ClickListener {
    public void onClick(View view, int position);

    public void onLongClick(View view, int position);
}
