package com.example.ozh.printme20.Model;

import com.example.ozh.printme20.InstagrammPiUtils.JsonMarks;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by oleg on 25.08.15.
 *
 * Модель Фотографии. Парсим ссылки на оба разрешения фотографии
 * Количество лайков
 * Время загрузки фото. Надо умножать на 1000, перед парсингом в формат даты, иначе будет 17.01.1975
 * Фильтр (- А что это?
 *         - я не в курсе. Судя по ТЗ - НАДО.)
 * Bitmap из ссылки на lowResolution для того, чтобы коллаж клеить проще было.
 * boolean переменная для адаптера, чтобы он знал, где ставить галочку на фото.
 */
public class Image {

    private String mStandartResolution;
    private String mLowResolution;
    private int mLikes;
    private long mDate;
    private String mFilter;
    private boolean mChecked;

    public Image(JSONObject image, int likes, long date, String filter, boolean checked) throws JSONException
    {
        this.mStandartResolution = image.getJSONObject(JsonMarks.STANDART_RESOLUTION).getString(JsonMarks.URL);
        this.mLowResolution = image.getJSONObject(JsonMarks.LOW_RESOLUTION).getString(JsonMarks.URL);
        this.mLikes = likes;
        this.mDate = date;
        this.mFilter = filter;
        this.mChecked = checked;
    }

    public String getStandartResolution() {
        return mStandartResolution;
    }

    public void setStandartResolution(String mStandartResolution) {
        this.mStandartResolution = mStandartResolution;
    }

    public String getLowResolution() {
        return mLowResolution;
    }

    public void setLowResolution(String mLowResolution) {
        this.mLowResolution = mLowResolution;
    }

    public int getLikes() {
        return mLikes;
    }

    public void setLikes(int mLikes) {
        this.mLikes = mLikes;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long mDate) {
        this.mDate = mDate;
    }

    public String getFilter() {
        return mFilter;
    }

    public void setFilter(String mFilter) {
        this.mFilter = mFilter;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean mChecked) {
        this.mChecked = mChecked;
    }

}
