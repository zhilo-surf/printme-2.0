package com.example.ozh.printme20.InstagrammPiUtils;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.ozh.printme20.Model.User;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oleg on 23.08.15.
 * Таск для поиска Юзеров
 */
public class SearchUserTask extends AsyncTask<String, Void, SearchResult> {

    private List<User> users = new ArrayList<>();
    private static String TAG = SearchUserTask.class.getSimpleName();
    private CompleteSearchListener callback;

    public SearchUserTask(CompleteSearchListener callback) {
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected SearchResult doInBackground(String... name) {
        try {
            String line = Utils.getResponseString(Api.MAIN_URL + Api.SEARCH + name[0] + Api.CLIENT_ID);
            JSONObject ob = new JSONObject(line);
            Log.d(TAG, line);
            JSONObject meta = ob.getJSONObject(JsonMarks.META);
            JSONArray data = ob.getJSONArray(JsonMarks.DATA);
                        
            if(meta.getInt(JsonMarks.CODE) == 200){
                if(data.length() == 0){
                    return SearchResult.NO_USER;
                }
                for (int i = 0; i < data.length(); i++){
                    if(isCancelled()){
                        return null;
                    }
                    JSONObject user = (JSONObject) data.get(i);
                    Log.d(TAG, user.toString());
                    users.add(new User(user));
                }

                return SearchResult.OK;
            }else return SearchResult.NO_USER;

        }  catch (JSONException e) {
            e.printStackTrace();
        }
        return SearchResult.ERROR;
    }

    @Override
    protected void onPostExecute(SearchResult result) {
        super.onPostExecute(result);

        switch (result)
        {
            case ERROR:
                callback.onCompleteSearch(users, SearchResult.ERROR);
                break;
            case OK:
                callback.onCompleteSearch(users, SearchResult.OK);
                break;
            case NO_USER:
                callback.onCompleteSearch(users, SearchResult.NO_USER);
                break;
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        callback.onCompleteSearch(null, null);
    }
}
