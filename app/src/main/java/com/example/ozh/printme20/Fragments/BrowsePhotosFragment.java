package com.example.ozh.printme20.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.ozh.printme20.Adapters.UserPhotoListAdapter;
import com.example.ozh.printme20.InstagrammPiUtils.ClickListener;
import com.example.ozh.printme20.InstagrammPiUtils.CompleteLoadingListener;
import com.example.ozh.printme20.InstagrammPiUtils.JsonMarks;
import com.example.ozh.printme20.InstagrammPiUtils.RecyclerTouchListener;
import com.example.ozh.printme20.InstagrammPiUtils.SearchResult;
import com.example.ozh.printme20.InstagrammPiUtils.UserPhotoLoader;
import com.example.ozh.printme20.InstagrammPiUtils.Utils;
import com.example.ozh.printme20.Model.Image;
import com.example.ozh.printme20.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by oleg on 25.08.15.
 */
public class BrowsePhotosFragment extends Fragment implements CompleteLoadingListener {

    private static String TAG = BrowsePhotosFragment.class.getSimpleName();
    private static String userId;
    private RecyclerView mRecyclerView;
    private UserPhotoListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserPhotoLoader userPhotoLoader;
    private List<Image> imageList;
    private Toolbar toolbar;
    private Button imageButton;
    private ActionBar actionBar;
    public static List<Image> photos = new ArrayList<>();
    private static ArrayList<Integer> positions = new ArrayList<Integer>();
    private final int countPhotos = 4;
    private ProgressDialog progressDialog;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private CollageMakerTask task;
    private View v;
    private Handler handler = new Handler();

    public static BrowsePhotosFragment newInstance(Bundle bundleId){
        BrowsePhotosFragment browsePhotosFragment = new BrowsePhotosFragment();
        browsePhotosFragment.setArguments(bundleId);
        return browsePhotosFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        userId = getArguments().getString(JsonMarks.USER_ID);
        userPhotoLoader = new UserPhotoLoader(BrowsePhotosFragment.this);
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        userPhotoLoader.execute(userId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_browse_photos, container, false);
        Log.d(TAG, "onCreateView");
        initDialog();
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        initToolbar();
        initFab(v);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view_photos);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Utils.checkNetworkConnection(getActivity())) {
                            if (!photos.contains(imageList.get(position))) {
                                if (photos.size() < countPhotos) {
                                    photos.add(imageList.get(position));
                                    positions.add(position);
                                    actionBarTitle(photos.size(), v);
                                    if (imageList.get(position).isChecked()) {
                                        imageList.get(position).setChecked(false);
                                    } else {
                                        imageList.get(position).setChecked(true);
                                    }
                                    mAdapter.notifyDataSetChanged();
                                }
                            } else {
                                photos.remove(imageList.get(position));
                                positions.remove((Object) position);
                                actionBarTitle(photos.size(), v);
                                if (imageList.get(position).isChecked()) {
                                    imageList.get(position).setChecked(false);
                                } else {
                                    imageList.get(position).setChecked(true);
                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 200);
//                if (!photos.contains(imageList.get(position))) {
//                    if (photos.size() < countPhotos) {
//                        photos.add(imageList.get(position));
//                        positions.add(position);
//                        actionBarTitle(photos.size(), v);
//                        if (imageList.get(position).isChecked()) {
//                            imageList.get(position).setChecked(false);
//                        } else {
//                            imageList.get(position).setChecked(true);
//                        }
//                        mAdapter.notifyDataSetChanged();
//                    }
//                } else {
//                    photos.remove(imageList.get(position));
//                    positions.remove((Object) position);
//                    actionBarTitle(photos.size(), v);
//                    if (imageList.get(position).isChecked()) {
//                        imageList.get(position).setChecked(false);
//                    } else {
//                        imageList.get(position).setChecked(true);
//                    }
//                    mAdapter.notifyDataSetChanged();
//                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return v;
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        photos.clear();
        positions.clear();
    }

    private void initToolbar() {
        ((ActionBarActivity)getActivity()).setSupportActionBar(toolbar);
        actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
            actionBar.setHomeAsUpIndicator(R.mipmap.back_bttn);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void initFab(View v) {
        v.findViewById(R.id.fabButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.checkNetworkConnection(getActivity())) {
                    if (task == null || !(task.getStatus() == AsyncTask.Status.RUNNING)) {
                        task = new CollageMakerTask();
                        task.execute();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.collage_has_already_making), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initDialog(){
        progressDialog = new ProgressDialog(getActivity(),
                R.style.AppTheme_Dark_Dialog );
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if(userPhotoLoader != null){
                    userPhotoLoader.cancel(true);
                }
            }
        });
        progressDialog.show();
    }

    private void actionBarTitle(int count, View v){
        if(count == 0){
            actionBar.setTitle("");
        }else {
            actionBar.setTitle(count + " из " + countPhotos);
        }

        if(count == 4){
            v.findViewById(R.id.fabButton).setVisibility(View.VISIBLE);
        }else {
            v.findViewById(R.id.fabButton).setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onCompleteSearch(List<Image> list, SearchResult searchResult) {
        imageList = list;
        if(searchResult == SearchResult.NO_USER){
            Toast.makeText(getActivity(), getString(R.string.no_users), Toast.LENGTH_SHORT).show();
        }else if(searchResult == SearchResult.OK){
            mAdapter = new UserPhotoListAdapter(getActivity(), imageList);
            mRecyclerView.setAdapter(mAdapter);
        }else if(searchResult == SearchResult.ERROR){
            Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
        }else if(list == null && searchResult == null){
            Toast.makeText(getActivity(), getString(R.string.cancel), Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
        }

        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }

    public class CollageMakerTask extends AsyncTask<Void, Void, byte[]>{

        private byte[] byteArray = null;
        ProgressDialog progressDialog;
        private HashMap<Integer, Bitmap> prepPhoto = new HashMap<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity(),
                    R.style.AppTheme_Dark_Dialog );
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.make_collage));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if(task != null){
                        task.cancel(true);
                    }
                }
            });
            progressDialog.show();
        }

        @Override
        protected byte[] doInBackground(Void... params) {
            try {
                for(Image image : photos){
                    if(isCancelled()){
                        return null;
                    }
                    prepPhoto.put(imageList.indexOf(image), imageLoader.loadImageSync(image.getStandartResolution()));
                }

                Bitmap bmOverlay = Bitmap.createBitmap(prepPhoto.get(positions.get(0)).getWidth()*2,
                        prepPhoto.get(positions.get(0)).getHeight()*2,
                        prepPhoto.get(positions.get(0)).getConfig());
                Canvas canvas = new Canvas(bmOverlay);
                canvas.drawBitmap(prepPhoto.get(positions.get(0)), 0f, 0f, null);
                canvas.drawBitmap(prepPhoto.get(positions.get(1)), prepPhoto.get(positions.get(0)).getWidth(), 0f, null);
                canvas.drawBitmap(prepPhoto.get(positions.get(2)), 0f, prepPhoto.get(positions.get(0)).getHeight(), null);
                canvas.drawBitmap(prepPhoto.get(positions.get(3)), prepPhoto.get(positions.get(0)).getHeight(), prepPhoto.get(positions.get(0)).getWidth(), null);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmOverlay.compress(Bitmap.CompressFormat.PNG, 0, stream);
                byteArray = stream.toByteArray();
            } catch (NullPointerException e) {
                e.printStackTrace();
                return byteArray;
            }
            return byteArray;
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            super.onPostExecute(bytes);
            if(progressDialog !=null){
                progressDialog.dismiss();
            }
            if(bytes != null) {
                FragmentManager fm = getFragmentManager();
                SendDialogFragment dF = new SendDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putByteArray("pathOfBmp", byteArray);
                dF.setArguments(bundle);
                dF.show(fm, "dlg");

            }else{
                Toast.makeText(getActivity(), getString(R.string.uncollage), Toast.LENGTH_SHORT).show();
                //getActivity().onBackPressed();
            }

            for(Image image : imageList){
                image.setChecked(false);
            }
            positions.clear();
            photos.clear();
            mAdapter.notifyDataSetChanged();
            actionBar.setTitle("");
            v.findViewById(R.id.fabButton).setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(getActivity(), getString(R.string.uncollage), Toast.LENGTH_SHORT).show();
        }
    }

}
