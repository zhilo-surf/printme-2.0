package com.example.ozh.printme20.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ozh.printme20.R;

/**
 * Created by oleg on 25.08.15.
 */
public class ViewPhotoHolder extends RecyclerView.ViewHolder {

    public ImageView imageView;
    public ImageView checkCyrcle;
    public TextView date;
    public TextView filterName;
    public TextView countLike;

    public ViewPhotoHolder(View view) {
        super(view);
        imageView = (ImageView) view.findViewById(R.id.user_photo);
        checkCyrcle = (ImageView) view.findViewById(R.id.check_cyrcle);
        date = (TextView) view.findViewById(R.id.photo_date);
        filterName = (TextView) view.findViewById(R.id.filter_name);
        countLike = (TextView) view.findViewById(R.id.countlike);
    }
}
