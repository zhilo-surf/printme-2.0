package com.example.ozh.printme20;

import android.app.Fragment;
import android.os.Bundle;

import com.example.ozh.printme20.Fragments.BrowsePhotosFragment;
import com.example.ozh.printme20.InstagrammPiUtils.JsonMarks;

public class BrowsePhotosActivity extends SingleFragmentActivity {


    @Override
    protected Fragment createFragment() {
        Bundle bundle = getIntent().getBundleExtra(JsonMarks.USER_ID);
        return new BrowsePhotosFragment().newInstance(bundle);
    }
}
