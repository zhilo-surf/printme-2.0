package com.example.ozh.printme20.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.ozh.printme20.Model.Image;
import com.example.ozh.printme20.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by oleg on 25.08.15.
 */
public class UserPhotoListAdapter extends RecyclerView.Adapter<ViewPhotoHolder> {


    private Context context;
    private List<Image> images;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public UserPhotoListAdapter(Context context, List<Image> images) {
        this.context = context;
        this.images = images;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_instagram_black_48dp)
                .showImageForEmptyUri(R.drawable.ic_instagram_grey600_48dp)
                .showImageOnFail(R.drawable.ic_instagram_grey600_48dp)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    @Override
    public ViewPhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item, parent, false);
        ViewPhotoHolder viewHolder = new ViewPhotoHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewPhotoHolder holder, int position) {
        holder.filterName.setText(images.get(position).getFilter());
        holder.countLike.setText(String.valueOf(images.get(position).getLikes()));
        holder.countLike.setAlpha(0.87f);
        holder.date.setText(millsToDate(images.get(position).getDate()));
        holder.date.setAlpha(0.54f);
        holder.imageView.setColorFilter(Color.parseColor("#6A000000"), PorterDuff.Mode.DARKEN);
        imageLoader.displayImage(images.get(position).getLowResolution(), holder.imageView, options);

        if(images.get(position).isChecked()){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                holder.checkCyrcle.setImageDrawable(context.getDrawable(R.drawable.check_1_icon));
            }else{
                holder.checkCyrcle.setImageDrawable(context.getResources().getDrawable(R.drawable.check_1_icon));
            }
        }else{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                holder.checkCyrcle.setImageDrawable(context.getDrawable(R.drawable.check_0_icon));
            }else{
                holder.checkCyrcle.setImageDrawable(context.getResources().getDrawable(R.drawable.check_0_icon));
            }
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    private String millsToDate(long timeMills){
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy", Locale.UK);
        String dateString = formatter.format(new Date(timeMills * 1000));
        return dateString.toLowerCase();
    }
}
