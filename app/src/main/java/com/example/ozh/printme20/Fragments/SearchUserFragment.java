package com.example.ozh.printme20.Fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ozh.printme20.InstagrammPiUtils.JsonMarks;
import com.example.ozh.printme20.InstagrammPiUtils.SearchResult;
import com.example.ozh.printme20.InstagrammPiUtils.SearchUserTask;
import com.example.ozh.printme20.InstagrammPiUtils.CompleteSearchListener;
import com.example.ozh.printme20.InstagrammPiUtils.Utils;
import com.example.ozh.printme20.Model.User;
import com.example.ozh.printme20.R;
import com.example.ozh.printme20.UserListActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchUserFragment extends Fragment implements CompleteSearchListener {

    private EditText editText;
    private View button;
    private SearchUserTask searchUserTask;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        editText = (EditText) view.findViewById(R.id.edit_search);
        button = view.findViewById(R.id.card_view);

        initializeWidget();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        button.setEnabled(true);
    }

    public void initializeWidget(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setEnabled(false);
                performSearch();
            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    button.setEnabled(false);
                    performSearch();
                    return true;
                }
                return false;
            }
        });
    }

    private void performSearch() {
        if (Utils.checkNetworkConnection(getActivity())) {
            if (editText.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.empty_nickname), Toast.LENGTH_SHORT).show();
                button.setEnabled(true);
            } else {
                Utils.hideSoftKeyboard(getActivity());
                initDialog();
                searchUserTask = new SearchUserTask(SearchUserFragment.this);
                searchUserTask.execute(editText.getText().toString());
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_LONG).show();
            button.setEnabled(true);
        }
    }

    private void initDialog(){
        progressDialog = new ProgressDialog(getActivity(),
                R.style.AppTheme_Dark_Dialog );
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.search_dialog_message));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if(searchUserTask != null){
                    searchUserTask.cancel(true);
                }
            }
        });
        progressDialog.show();
    }

    @Override
    public void onCompleteSearch(List<User> list, SearchResult searchResult) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }

        if(searchResult == SearchResult.NO_USER){
            Toast.makeText(getActivity(), getString(R.string.no_users), Toast.LENGTH_SHORT).show();
        }else if(searchResult == SearchResult.OK){
            Intent intent = new Intent(getActivity(), UserListActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(JsonMarks.USERS, (ArrayList<? extends Parcelable>) list);
            intent.putExtra(JsonMarks.USERS, bundle);
            startActivity(intent);
        }else if(searchResult == SearchResult.ERROR){
            Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
        }else if(list == null && searchResult == null){
            Toast.makeText(getActivity(), getString(R.string.cancel), Toast.LENGTH_SHORT).show();
            button.setEnabled(true);
        }
    }
}
