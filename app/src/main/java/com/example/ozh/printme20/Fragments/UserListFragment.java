package com.example.ozh.printme20.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ozh.printme20.Adapters.UserListAdapter;
import com.example.ozh.printme20.BrowsePhotosActivity;
import com.example.ozh.printme20.InstagrammPiUtils.ClickListener;
import com.example.ozh.printme20.InstagrammPiUtils.JsonMarks;
import com.example.ozh.printme20.InstagrammPiUtils.RecyclerTouchListener;
import com.example.ozh.printme20.InstagrammPiUtils.Utils;
import com.example.ozh.printme20.Model.User;
import com.example.ozh.printme20.R;

import java.util.List;

/**
 * Created by oleg on 25.08.15.
 */
public class UserListFragment extends Fragment {

    private static List<User> list;
    private RecyclerView mRecyclerView;
    private UserListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Handler handler = new Handler();

    public static UserListFragment newInstance(Bundle bundle){
        UserListFragment userListFragment = new UserListFragment();
        userListFragment.setArguments(bundle);
        return userListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = getArguments().getParcelableArrayList(JsonMarks.USERS);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_list, container, false);
        initToolbar(v);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new UserListAdapter(getActivity(), list);
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(Utils.checkNetworkConnection(getActivity())) {
                            Intent intent = new Intent(getActivity(), BrowsePhotosActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(JsonMarks.USER_ID, list.get(position).getId());
                            intent.putExtra(JsonMarks.USER_ID, bundle);
                            getActivity().startActivity(intent);
                        }else{
                            Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 64);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        mRecyclerView.setAdapter(mAdapter);
        return v;
    }

    private void initToolbar(View view) {
        final Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((ActionBarActivity)getActivity()).setSupportActionBar(toolbar);
        final ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.have_found) + " " + list.size());
            actionBar.setHomeAsUpIndicator(R.mipmap.back_bttn);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }
}
