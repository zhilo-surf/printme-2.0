package com.example.ozh.printme20.InstagrammPiUtils;

import com.example.ozh.printme20.Model.User;

import java.util.List;

/**
 * Created by oleg on 24.08.15.
 */
public interface CompleteSearchListener {
    public void onCompleteSearch(List<User> list, SearchResult searchResult);
}
