package com.example.ozh.printme20.InstagrammPiUtils;


import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.example.ozh.printme20.Model.Image;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by oleg on 25.08.15.
 * Таск для парсинга фото из JsonObject
 */
public class UserPhotoLoader extends AsyncTask<String, Void, SearchResult> {

    private static String TAG = UserPhotoLoader.class.getSimpleName();
    private CompleteLoadingListener callback;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private List<Image> images = new ArrayList<Image>();

    public UserPhotoLoader(CompleteLoadingListener completeLoadingListener) {
        this.callback = completeLoadingListener;

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    protected SearchResult doInBackground(String... name) {

        String nextUrl = null;
        String jsonString = Utils.getResponseString(Api.MAIN_URL + name[0] + Api.MEDIA + Api.CLIENT_ID_RECENT);

        JSONObject jsonPhotos = null;
        try {
            jsonPhotos = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try{
            do{
                if(isCancelled()){
                    return null;
                }

                jsonPhotos = new JSONObject(jsonString);
                if(jsonString != null) {
                    if (jsonPhotos.getJSONObject(JsonMarks.PAGINATION).length() != 0) {
                        JSONObject pagination = jsonPhotos.getJSONObject(JsonMarks.PAGINATION);
                        nextUrl = pagination.get(JsonMarks.NEXT_URL).toString();
                        if(isCancelled()){
                            return null;
                        }
                        jsonString = Utils.getResponseString(nextUrl);
                    }
                    JSONObject meta = new JSONObject(jsonString).getJSONObject(JsonMarks.META);
                    JSONArray jsonArrayData = jsonPhotos.getJSONArray(JsonMarks.DATA);
                    if (meta.getInt("code") == 200) {
                        if (jsonArrayData.length() == 0) {
                            return SearchResult.NO_USER;
                        }
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject nowObj = (JSONObject) jsonArrayData.get(i);
                            if (nowObj.getString(JsonMarks.TYPE).equalsIgnoreCase(JsonMarks.IMAGE)) {
                                images.add(new Image(nowObj.getJSONObject(JsonMarks.IMAGES),
                                        nowObj.getJSONObject(JsonMarks.LIKES).getInt(JsonMarks.COUNT),
                                        nowObj.getLong(JsonMarks.CREATED_TIME),
                                        nowObj.getString(JsonMarks.FILTER),
                                        false));
                            }
                        }
                    }else{
                        return SearchResult.ERROR;
                    }
                }

            }while ((jsonPhotos.getJSONObject(JsonMarks.PAGINATION).length() != 0));
        }catch (JSONException e){
            e.printStackTrace();
        }finally {
            Collections.sort(images, new ImageComparator());
            return SearchResult.OK;
        }
    }

    @Override
    protected void onPostExecute(SearchResult result) {
        super.onPostExecute(result);
        switch (result)
        {
            case ERROR:
                callback.onCompleteSearch(images, SearchResult.ERROR);
                break;
            case OK:
                callback.onCompleteSearch(images, SearchResult.OK);
                break;
            case NO_USER:
                callback.onCompleteSearch(images, SearchResult.NO_USER);
                break;
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        callback.onCompleteSearch(null, null);
    }

    public class ImageComparator implements Comparator<Image>{
        @Override
        public int compare(Image lhs, Image rhs) {
            return rhs.getLikes() - lhs.getLikes();
        }
    }
}


