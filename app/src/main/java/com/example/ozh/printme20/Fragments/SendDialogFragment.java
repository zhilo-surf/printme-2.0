package com.example.ozh.printme20.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.DialogFragment;
import android.support.v4.print.PrintHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.example.ozh.printme20.R;


public class SendDialogFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Bundle args = getArguments();

        byte[] byteArray = args.getByteArray("pathOfBmp");
        final Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_dialog, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setImageBitmap(bmp);
        builder.setView(view)
                .setPositiveButton(getString(R.string.print), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                            PrintHelper photoPrinter = new PrintHelper(getActivity());
                            photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
                            Bitmap bitmap = bmp;
                            photoPrinter.printBitmap("InstaCollage.jpg", bitmap);
                        }else {
                            String pathOfBmp = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bmp, "InstaCollage", null);
                            Uri bmpUri = Uri.parse(pathOfBmp);
                            Intent emailIntent = new Intent(Intent.ACTION_SEND);
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Test");
                            emailIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            emailIntent.setType("image/png");
                            startActivity(emailIntent);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.back), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        return builder.show();
    }
}
