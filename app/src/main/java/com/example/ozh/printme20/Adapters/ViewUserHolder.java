package com.example.ozh.printme20.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ozh.printme20.R;

/**
 * Created by oleg on 25.08.15.
 */
public class ViewUserHolder extends RecyclerView.ViewHolder{

    public ImageView imageView;
    public TextView textView;

    public ViewUserHolder(View itemView) {
        super(itemView);
        imageView = (ImageView)itemView.findViewById(R.id.img_thumbnail);
        textView = (TextView) itemView.findViewById(R.id.person_name);
    }

}
