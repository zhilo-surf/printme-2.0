package com.example.ozh.printme20.InstagrammPiUtils;

/**
 * Created by oleg on 24.08.15. *
 * Переменные для парсинка json с Instagram
 */
public interface JsonMarks {
    String DATA = "data";
    String META = "meta";
    String CODE = "code";
    String TYPE = "type";
    String IMAGE = "image";
    String IMAGES = "images";
    String LIKES = "likes";
    String COUNT = "count";
    String CREATED_TIME = "created_time";
    String FILTER = "filter";
    String LOW_RESOLUTION = "low_resolution";
    String STANDART_RESOLUTION = "standard_resolution";
    String URL = "url";
    String PAGINATION = "pagination";
    String NEXT_URL = "next_url";

    String USERS = "users";
    String USER_ID = "id";
}
