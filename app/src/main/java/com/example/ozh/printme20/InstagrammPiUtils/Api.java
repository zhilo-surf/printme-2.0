package com.example.ozh.printme20.InstagrammPiUtils;

/**
 * Created by oleg on 25.08.15.
 */
public interface Api {
    /**
     * Api для запросов
     * */
    public static final String MAIN_URL = "https://api.instagram.com/v1/users/";
    public static final String SEARCH = "search?q=";
    public static final String CLIENT_ID = "&client_id=c774209e82b141a2bb564756fcf3a953";
    public static final String MEDIA = "/media/recent/";
    public static final String CLIENT_ID_RECENT = "?client_id=c774209e82b141a2bb564756fcf3a953";
}
