package com.example.ozh.printme20.Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by oleg on 23.08.15.
 */
public class User implements Parcelable {

    private String mProfImage;
    private String mId;
    private String mName;

    public User (JSONObject json) throws JSONException
    {
        this.mName = json.getString("username");
        this.mId = json.getString("id");
        this.mProfImage = json.getString("profile_picture");
    }

    public String getName() {
        return mName;
    }

    public String getId() {
        return mId;
    }

    public String getProfImage() {
        return mProfImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mProfImage);
        dest.writeString(mId);
        dest.writeString(mName);
    }

    public static Parcelable.Creator<User> CREATOR
            = new Parcelable.Creator<User>(){

        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User(Parcel in){
        this.mProfImage = in.readString();
        this.mId = in.readString();
        this.mName = in.readString();
    }
}
